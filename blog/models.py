from django.db import models

class Blog(models.Model):
    title = models.CharField(max_length=120)
    pub_date = models.DateTimeField(auto_now=True)
    body = models.TextField()
    image = models.ImageField(upload_to='images/')